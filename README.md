# OMN - Own Made Network

## Was ist das OMN?
Das OMN ist ein Netzwerk von Studenten, welche anderen Studenten das Studieren erleichtern. Dazu werden Skripten, Lehrinhalte, Übungen, Beispiele und Lösungen in einer aufgearbeiteten Form zur Verfügung gestellt.

## Was ist das Ziel vom OMN?
Das OMN möchte sämtliche Inhalte für jede Lehrveranstaltung digitalisieren, damit diese in einer sauberen Form als Dokument erstellt werden können. Sind diese Inhalte digital vorhanden, können Korrekturen und Erweiterungen leichter und schneller vorgenommen werden, als an bestehenden handschriftlichen Lösungen und Zusammenfassungen. Dabei soll besonders darauf acht gelegt werden, dass über alle Lehrveranstaltungen hinweg eine konsistente Definition von Variablen und Namen eingehalten wird, um das Lernen zu erleichtern. Die Lernunterlagen sollen untereinander kompatibel sein.

## Warum gibt es für jede Lehrveranstaltung ein eigenes Git Repo (Repository)?
Der Sinn hinter den Git Repos liegt darin, dass mit nur einem Befehl (git clone URL) alle freien Informationen einer Lehrveranstaltung auf deinen lokalen PC heruntergeladen werden können, und nicht mühsam aus verschiedensten Quellen zusammengesucht werden müssen und mit dem Befehl (git pull upstream master) du sofort auf dem aktuellsten Stand bist.

## Was ist der Unterschied zwischen dem opn (open) und cld (closed) Ordner?
Der opn (open) Ordner ist für jeden einsehbar und unterliegt der GNU GPLv3. Jeder kann hier stöbern und sich ansehen, was das OMN zur Verfügung stellt und neue Inhalte dazu beitragen. Der cld (closed) Ordner ist nur für OMN Mitglieder zugänglich, welche sich am OMN beteiligen.

## Warum gibt es einen open und einen closed Ordner?
Das OMN soll wachsen und dazu benötigt es die Unterstützung von Studenten. Um zu verhindern, dass die aufwendig erstellten Zusammenfassungen und Beispielsammlungen von Studenten nur zum eigenen Vorteil verwendet werden, ohne etwas dem OMN zurück zu geben, gibt es einen closed Ordner.

# Mitglied

## Wie werde ich Mitglied?
Das ist ganz einfach. Schau in die TODO Datei und suche dir eine Aufgabe aus, welche noch nicht erledigt wurde. Wenn du zwei Punkte gesammelt hast, bekommst du die aktuellsten Lernunterlagen mit den verfügbaren Lösungen an deine e-mail adresse geschickt.

## Wie kann ich etwas beitragen?
Schau dazu in die CONTRIBUTING Datei.

# Lernunterlagen
Ich habe zwei Punkte erreicht, wo finde ich die Lernunterlagen?

## Alte Releases auf GitLab
Jedes Quartal wird ein Release in reduziertem Umfang auf GitLab zur Verfügung gestellt. Diesen kannst du nutzen und herunterladen ohne jeglich Gegenleistung.

## Anfragen
Nachdem du zwei Punkte gesammelt hast, kannst du entweder eine Anfrage im et-forum an Painkilla schicken, in dem betreffenden Thread einen Post oder ein Issue auf GitLab erstellen oder du schreibst in die Telegram Gruppe des OMN. https://t.me/joinchat/FGFedBNMoUkUMscg8a0fgQ

## Selber kompilieren
Geh dazu in den Hauptordner des Repo und führe den nachfolgenden Befehl aus. Kompilieren geht derzeit auf Linux mit installiertem Latex und Zusatzpaketen. Für Windows gibt es [hier](https://gitlab.com/Painkilla/conf/blob/master/tut/cygwin/cygwin.md) einen Workaround. Die closed Inhalte können nur kompiliert werden, wenn du den notwendigen Zugriff auf die Repos hast und diese auf deinen PC heruntergeladen hast.

```
$ make all
```
    
    
# FAQ
Allgemeine Fragen

## Muss ich bei jeder Lehrveranstaltung etwas beitragen um für den closed Bereich freigeschalten zu werden?
NEIN. Das Ziel ist, dass du bei der ersten Lehrveranstaltung eine Aufgabe im open Bereich erfüllst, und dann beliebig viele Aufgaben im closed Bereich. Je mehr Aufgaben du im closed Bereich erfüllst, desto mehr closed Inhalte bekommst du von anderen Lehrveranstaltung ohne dafür auch nur irgendetwas machen zu müssen.

## Ich bin gerade so im Stress und kann leider nichts beitragen, aber wenn ich die Prüfung bestanden habe, werde ich etwas Beitragen. Könntet Ihr mir bitte die Dokumente vorab schicken?
NEIN, alternativ kannst du auch für eine Prüfung, die du bereits gemacht hast, Material hochladen und dir damit die Freischaltung für diese Prüfung holen.

## Ich möchte gerne etwas im closed Bereich beitragen, bin aber dafür leider noch nicht freigeschaltet.
Netter Versuch. Zuerst im open Bereich, dann im closed. 

## Darf ich die Lernunterlagen mit Freunden teilen oder Veröffentlichen?
NEIN, die Lehrunterlagen sind nur für dich persönlich gedacht. Damit hilfst du uns, den Anreiz von neuen Beiträgen hoch zu halten und das hilft letztendlich allen.

## Ich hab eine Frage zu einem Rechenbeispiel, Theorie, Mündliche Frage, usw.
Erstelle für jede Frage EIN Issue. In den Titel schreibst du hinein um welches Beispiel oder Theoriefrage, usw. es sich handelt. Ist deine Frage beantwortet, wird dein Issue geschlossen und die Lösung deiner Frage wird in das Repo eingepflegt. Hast du eine Frage zu einem Beispiel, dass bereits beantwortet ist/gelöst ist, kannst du das Issue wieder öffnen. Mit dieser Vorgehensweise wollen wir elend lange Threads vermeiden, wo komplett chaotisch über mehrere Posts hinweg verschiedenste Probleme/Fragen behandelt werden und auch ein nachträgliches Lesen sehr aufwendig ist.
